#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <PubSubClient.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <LedControl.h>

const int DIN_PIN = D7;
const int CS_PIN = D6;
const int CLK_PIN = D5;

const char* mqtt_server = "MQTT_HOST";
const int mqtt_port = "MQTT_POR";
char hostString[16] = {0};
String espTopic = "";

WiFiClient espClient;
PubSubClient client(espClient);
LedControl display = LedControl(DIN_PIN, CLK_PIN, CS_PIN);

void setup() {
  pinMode(BUILTIN_LED, OUTPUT);
  Serial.begin(115200);
  sprintf(hostString, "NIGHTLIGHT_%06X", ESP.getChipId());

  setup_wifi();
  setup_mqtt();
  setup_service();
  setup_matrix();
}

void setup_wifi() {
  WiFiManager wifiManager;
  wifiManager.autoConnect(hostString);
}

void setup_mqtt() {
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }

  if ((char)payload[0] == '1') {
    digitalWrite(BUILTIN_LED, LOW);
  } else {
    digitalWrite(BUILTIN_LED, HIGH);
  }
  
  Serial.println();
  displayImage(payload);
}

void setup_service() {
  Serial.print("Hostname: ");
  Serial.println(hostString);
  WiFi.hostname(hostString);
  
  if (!MDNS.begin(hostString)) {
    Serial.println("Error setting up MDNS responder!");
  }
  
  MDNS.addService("nightliot", "tcp", 6666);
}

void setup_matrix() {
  display.clearDisplay(0);
  display.shutdown(0, false);
  display.setIntensity(0, 10);
}

void displayImage(byte* payload) {
  for (int row = 0; row < 8; row++) {
    for (int col = 0; col < 8; col++) {
      bool value = payload[row * 8 + col] == '0' ? 0 : 1;
      display.setLed(0, row, col, value);
    }
  }
}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");

    if (client.connect(hostString)) {
      Serial.println("connected");
      espTopic = "nightliot/" + String(hostString);
      client.subscribe(espTopic.c_str());
      Serial.print("Subscribed to topic: ");
      Serial.println(espTopic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  
  client.loop();
}
